# Domain Configuration

The following environment variables must be set:

* `NAMECHEAP_USER_NAME`
* `NAMECHEAP_API_KEY`
* `NAMECHEAP_API_USER`

The IP from which the workspace is applied must be manually whitelisted through
the Namecheap web UI.
