resource "namecheap_domain_records" "ascend-photography" {
  domain = "ascend.photography"
  mode = "OVERWRITE"

  # Google Workspace

  # subdomains
  record {
    type = "CNAME"
    hostname = "calendar"
    address = "ghs.googlehosted.com."
  }
  record {
    type = "CNAME"
    hostname = "drive"
    address = "ghs.googlehosted.com."
  }
  record {
    type = "CNAME"
    hostname = "groups"
    address = "ghs.googlehosted.com."
  }
  record {
    type = "CNAME"
    hostname = "mail"
    address = "ghs.googlehosted.com."
  }
  record {
    type = "CNAME"
    hostname = "sites"
    address = "ghs.googlehosted.com."
  }

  # verification
  record {
    type = "TXT"
    hostname = "@"
    address = "google-site-verification=G82GJPu6oZO0cJclLWy-qN-vgLy1l7B60iubNPDUXfk"
    ttl = 3600
  }

  # Email

  # verification
  record {
    type = "TXT"
    hostname = "google._domainkey"
    address = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAi3NvD8kdl1HhuqWZ1gOuE0MRkzzEnntdL6emUAn2z1unXDFyJNaREYumO/sCyYNDRidAfrdrz0F7vpKFtI/4h5RUszrgyW/XuZ79AIQHarV7zjcN3PH0K/ollOGn2Yi1uPqk44dFXaTMg66GqmgY6flzksXYYxyOYkv9xso3/3IrihhpBJK2phTYehLuS0OP3xB0N8Zh/HgXWhczVcS+gviNOL7Ez++MneCdFIPG2ZhGVc49FMtU7C27+cNuB/yA0OqU2As4KGGImOFg9Gri0VwPJrEUXF7IaS7o0oBizc0XFznzCosbT6ndwqaDHl8rVeSQClwIZQDhzXUbDRqgqQIDAQAB"
  }
}
