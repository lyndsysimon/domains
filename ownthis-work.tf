resource "namecheap_domain_records" "ownthis-work" {
  domain = "ownthis.work"
  mode = "OVERWRITE"
  email_type = "NONE"

  record {
    hostname = "www"
    type = "CNAME"
    address = "parkingpage.namecheap.com."
  }

  record {
    hostname = "@"
    type = "URL"
    address = "http://www.ownthis.work/"
  }
}
