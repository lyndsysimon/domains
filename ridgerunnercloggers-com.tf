resource "namecheap_domain_records" "ridgerunnercloggers-com" {
  domain = "ridgerunnercloggers.com"
  mode = "OVERWRITE"

  nameservers = [
    "ns6.wixdns.net",
    "ns7.wixdns.net",
  ]
}
