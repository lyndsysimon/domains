resource "namecheap_domain_records" "lyndsysimon-com" {
  domain = "lyndsysimon.com"
  mode = "OVERWRITE"
  email_type = "MX"

  # Blog

  record {
    type = "ALIAS"
    hostname = "@"
    address = "shaped-gull-448k3e3gisl09irype6qbwjy.herokudns.com."
    ttl = 300
  }
  record {
    type = "ALIAS"
    hostname = "www"
    address = "vertical-tuna-ghe2n96m1buy64g9o8vk4p8f.herokudns.com."
    ttl = 300
  }

  # Subdomains

  record {
    type = "CNAME"
    hostname = "notes"
    address = "pages.sr.ht."
  }

  # Email

  record {
    type = "MX"
    hostname = "@"
    address = "mail.protonmail.ch."
    mx_pref = 10
  }
  record {
    type = "TXT"
    hostname = "_dmarc"
    address = "v=DMARC1; p=none; rua=mailto:lyndsy@lyndsysimon.com"
  }
  record {
    type = "TXT"
    hostname = "protonmail._domainkey"
    address = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDSk3UL12ctjQxDkjKWHFJkMRjDDt3ebhqw3x27dYVNlpRfnTHtfwWKNhwlN/SlDddCgpuIHXQr0mtelUVAW8bdqXHuSRqCWl2mSdQb6A9n0NnNw8PPKBEsvsnwilOEedD5rY0Ht0ySk5NdUwNSdTh0wFhPKNGoi3nh9SKodBGNIQIDAQAB"
  }
  record {
    type = "TXT"
    hostname = "@"
    address = "protonmail-verification=5e7d5978c686d98f051954699786594667816165"
  }
  record {
    type = "TXT"
    hostname = "@"
    address = "v=spf1 include:_spf.protonmail.ch mx ~all"
  }

  # Domain Verifications

  # Brave Ledger
  record {
    type = "TXT"
    hostname = "@"
    address = "brave-ledger-verification=5c0b53ad16a3bc8a6b935cc0d01c9c186987724b9937219402f52f4c6926e776"
  }
}
