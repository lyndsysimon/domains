terraform {
  backend "local" {
    path = "terraform.tfstate"
  }

  required_providers {
    namecheap = {
      source = "namecheap/namecheap"
      version = ">= 2.0.0"
    }
  }
}
